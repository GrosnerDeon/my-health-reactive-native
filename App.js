import * as SplashScreen from 'expo-splash-screen';
import React from 'react';

import { MainScreen } from './components/mainScreen';
import { AnimatedAppLoader } from './components/animatedAppLoader'


SplashScreen.preventAutoHideAsync();

export default function App() {
  return (
    <AnimatedAppLoader image={require("./assets/heartbeatLoading.gif")}>
      <MainScreen />
    </AnimatedAppLoader>
  );
}