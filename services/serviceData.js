import AsyncStorage from '@react-native-async-storage/async-storage';

const bloodPressureRate = {
    MALE: {
        age: {
            20: {
                sys: {
                    min: 113,
                    max: 133
                },
                dia: {
                    min: 66,
                    max: 86
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            30: {
                sys: {
                    min: 119,
                    max: 139
                },
                dia: {
                    min: 71,
                    max: 91
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            40: {
                sys: {
                    min: 120,
                    max: 145
                },
                dia: {
                    min: 73,
                    max: 93
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            50: {
                sys: {
                    min: 120,
                    max: 152
                },
                dia: {
                    min: 75,
                    max: 95
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            60: {
                sys: {
                    min: 120,
                    max: 152
                },
                dia: {
                    min: 75,
                    max: 95
                },
                pul: {
                    min: 60,
                    max: 100
                }
            }
        }
    },
    FEMALE: {
        age: {
            20: {
                sys: {
                    min: 106,
                    max: 126
                },
                dia: {
                    min: 62,
                    max: 82
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            30: {
                sys: {
                    min: 117,
                    max: 137
                },
                dia: {
                    min: 70,
                    max: 90
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            40: {
                sys: {
                    min: 120,
                    max: 147
                },
                dia: {
                    min: 74,
                    max: 94
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            50: {
                sys: {
                    min: 120,
                    max: 154
                },
                dia: {
                    min: 74,
                    max: 94
                },
                pul: {
                    min: 60,
                    max: 100
                }
            },
            60: {
                sys: {
                    min: 120,
                    max: 159
                },
                dia: {
                    min: 75,
                    max: 95
                },
                pul: {
                    min: 60,
                    max: 100
                }
            }
        }
    }
};

class ServiceData {
    toServer (data) {
        return fetch('http://192.168.0.104:5000/api/patientCard/application', {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
          });
    }

    async sendToServer (data) {
        try {
            let response = await this.toServer(data);
            return response;
          } catch (error) {
            return { message: `Application data wasn't saved to serve` }
          }
    }

    async storePersonalInfo (value) {
        try {
            const jsonValue = JSON.stringify(value);
            await AsyncStorage.setItem('@personal_info', jsonValue);
        } catch (error) {
        }
    }

    async getPersonalInfo () {
        try {
            //await AsyncStorage.removeItem('@personal_info')
            const jsonValue = await AsyncStorage.getItem('@personal_info');

            return jsonValue !== null ? JSON.parse(jsonValue) : null;
        } catch (error) {
        }
    }

    async storePressureMeasurement (value) {
        try {
          const jsonValue = JSON.stringify(value)
          await AsyncStorage.setItem('@pressure_measurement', jsonValue)
        } catch (error) {
        }
    }
      
    async getPressureMeasurement () {
        try {
          const jsonValue = await AsyncStorage.getItem('@pressure_measurement');
          return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch(error) {
        }
    }

    async deletePressureMeasurement (value, id) {
        try {
            let indexDelete = 0;
            
            value.forEach((value, index) => {
                if (value.id === id) {
                    indexDelete = index;
                }
            });
            
            value.splice(indexDelete, 1);

            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('@pressure_measurement', jsonValue)
          } catch(error) {
          }
    }

    setBloodPressureFromUser (user, value) {
        user.minSYS = bloodPressureRate[user.gender].age[value].sys.min;
        user.maxSYS = bloodPressureRate[user.gender].age[value].sys.max;
        user.minDIA = bloodPressureRate[user.gender].age[value].dia.min;
        user.maxDIA = bloodPressureRate[user.gender].age[value].dia.max;
        user.minPUL = bloodPressureRate[user.gender].age[value].pul.min;
        user.maxPUL = bloodPressureRate[user.gender].age[value].pul.max;
    }

    bloodPressureCheck (user) {
        if (user.age <= 20) {
            this.setBloodPressureFromUser(user, 20);
        } else if (user.age > 20 && user.age <= 30) {
            this.setBloodPressureFromUser(user, 30);
        } else if (user.age > 30 && user.age <= 40) {
            this.setBloodPressureFromUser(user, 40);
        } else if (user.age > 40 && user.age <= 50) {
            this.setBloodPressureFromUser(user, 50);
        } else if ((user.age > 50 && user.age <= 60) || user.age > 60) {
            this.setBloodPressureFromUser(user, 60);
        }
    }
}

export const serviceData = new ServiceData(); 