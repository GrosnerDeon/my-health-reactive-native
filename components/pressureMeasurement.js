import { Animated, FlatList, View, Text } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect, useRef } from 'react';
import Moment from 'moment';
import { Icon, LinearProgress } from 'react-native-elements';

import { ListElement } from './listElement';
import { serviceData } from '../services/serviceData';
import { InputFields } from './inputField';
import { styles } from './pressureMeasurementStyle';

export const PressureMeasurement = props => {
    const { 
        user,
        onUpdateUser,
        onModalViewOpen 
        } = props;

    const [diastolic, setDiastolic] = useState('');
    const [systolic, setSystolic] = useState('');
    const [pulse, setPulse] = useState('');
    const [pressureMeasurementList, setPressureMeasurementList] = useState([]);
    const [isSendToServer, setIsSendToServer] = useState(false);
    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        serviceData.getPressureMeasurement().then(value => {
        if (value) {
            setPressureMeasurementList([...value]);
        }
        })
    }, []);

    useEffect(() => {
        Animated.timing(
        fadeAnim,
        {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true
        }
        ).start();
    }, [fadeAnim])

    const myTextInput1 = React.createRef();
    const myTextInput2 = React.createRef();
    const myTextInput3 = React.createRef();

    const sendToServer = () => {
        setIsSendToServer(true);
        serviceData.sendToServer({
            user,
            pressureMeasurementList
        }).then(response => {
            if (response && response.status === 201) {
                onModalViewOpen(`Application data was saved to server`)
            } else {
                onModalViewOpen(`Application data wasn't saved to server`)
            }
            setIsSendToServer(false);    
        }).catch(response => {
            onModalViewOpen(response.message);
            setIsSendToServer(false);
        });
    };

    const setPressureMeasurement = () => {
        if (diastolic !== '' && 
            systolic !== '' && 
            pulse !== '' && 
            !isNaN(Number(pulse)) && 
            !isNaN(Number(systolic)) && 
            !isNaN(Number(diastolic))) {
            serviceData.storePressureMeasurement([
                ...pressureMeasurementList,
                ...[{
                        diastolic,
                        systolic,
                        pulse,
                        date: Moment().format('DD/MM/YYYY HH:mm'),
                        id: `${ diastolic }_${ new Date().getTime() }`
                    }]]).then(() => {
                        serviceData.getPressureMeasurement().then(value => {
                            if (value) {
                                setPressureMeasurementList([...value]);
                                setSystolic('');
                                setDiastolic('');
                                setPulse('');
                                
                                if (myTextInput1 && myTextInput1.current) {
                                    myTextInput1.current.clear();
                                }

                                if (myTextInput2 && myTextInput2.current) {
                                    myTextInput2.current.clear();
                                }

                                if (myTextInput3 && myTextInput3.current) {
                                    myTextInput3.current.clear();
                                }
                            }
                        });
                    }); 
            }
    };

    const deletePressureMeasurement = item => {
        serviceData.deletePressureMeasurement(pressureMeasurementList, item.id).then(() => {
            serviceData.getPressureMeasurement().then(value => {
                if (value) {
                    setPressureMeasurementList([...value]);
                }
            });
        });
    };

    return (
        <View style={styles.mainContainer}>
            <Animated.View style={{...styles.mainContainer, opacity: fadeAnim}}>
                <View style={styles.containerPersonalInfo}>
                    {user.gender === 'MALE' ? 
                        <Icon
                            name='male'
                            type='font-awesome'
                            color='#ff5722'/> : 
                        <Icon
                            name='female'
                            type='font-awesome'
                            color='#ff5722'/>}
                    <Text style={styles.textContainerPersonalInfo}>
                        {user.firstName}
                    </Text>
                    <Text style={styles.textContainerPersonalInfo}>
                        {user.lastName}
                    </Text>
                    <Icon
                        raised
                        name='edit'
                        type='font-awesome'
                        color='#f50'
                        onPress={() => onUpdateUser()} />
                        <Icon
                        raised
                        name='cloud'
                        disabled={isSendToServer}
                        type='font-awesome'
                        color='#f50'
                        onPress={() => sendToServer()} />
                </View>
                <View style={isSendToServer ? 
                    styles.containerMainInput : styles.containerMainInputSendToServer}>
                    <View style={styles.containerInput}>
                        <InputFields 
                                text='SYS' 
                                textTooltip='Systolic'
                                maxLen={3} 
                                itemOnchange={systolic => setSystolic(systolic)}
                                defaultVal={systolic}
                                myTextInput={myTextInput1}/>
                        <InputFields 
                                text='DIA' 
                                textTooltip='Diastolic' 
                                maxLen={3}
                                itemOnchange={diastolic => setDiastolic(diastolic)}
                                defaultVal={diastolic}
                                myTextInput={myTextInput2}/>
                        <InputFields 
                                text='PUL' 
                                textTooltip='Pulse' 
                                maxLen={3}
                                itemOnchange={pulse => setPulse(pulse)}
                                defaultVal={pulse}
                                myTextInput={myTextInput3}/>
                        <Icon
                            raised
                            name='heartbeat'
                            type='font-awesome'
                            color='#f50'
                            onPress={() => setPressureMeasurement()} />
                    </View>
                    {isSendToServer && <View style={styles.progressBar}>
                        <LinearProgress color="#a73511"/>
                    </View>}
                </View>
                <View style={styles.containerList}>
                    <FlatList
                        style={{ backgroundColor: '#faebd7' }}
                        data = {pressureMeasurementList}
                        keyExtractor={item => `${item.id}`}
                        renderItem = {({ item }) => 
                            <View style={styles.container} key={item.id}>
                                {item.systolic >= user.minSYS && 
                                item.systolic <= user.maxSYS &&
                                    <Icon
                                        name='smile-o'
                                        type='font-awesome'
                                        color='#f50'/>}
                                {item.systolic < user.minSYS && 
                                    <Icon
                                        name='arrow-down'
                                        type='entypo'
                                        color='#f50'/>}
                                {item.systolic > user.maxSYS &&
                                    <Icon
                                        name='arrow-up'
                                        type='entypo'
                                        color='#f50'/>}
                                <ListElement 
                                    item={item.systolic} 
                                    text ='SYS' 
                                    textTooltip='Systolic'/>
                                {item.diastolic >= user.minDIA && 
                                item.diastolic <= user.maxDIA &&
                                    <Icon
                                        name='smile-o'
                                        type='font-awesome'
                                        color='#f50'/>}
                                {item.diastolic < user.minDIA && 
                                    <Icon
                                        name='arrow-down'
                                        type='entypo'
                                        color='#f50'/>}
                                {item.diastolic > user.maxDIA &&
                                    <Icon
                                        name='arrow-up'
                                        type='entypo'
                                        color='#f50'/>}
                                <ListElement 
                                    item={item.diastolic} 
                                    text ='DIA' 
                                    textTooltip='Diastolic'/>
                                {item.pulse >= user.minPUL && 
                                item.pulse <= user.maxPUL &&
                                    <Icon
                                        name='smile-o'
                                        type='font-awesome'
                                        color='#f50'/>}
                                {item.pulse < user.minPUL && 
                                    <Icon
                                        name='arrow-down'
                                        type='entypo'
                                        color='#f50'/>}
                                {item.pulse > user.maxPUL &&
                                    <Icon
                                        name='arrow-up'
                                        type='entypo'
                                        color='#f50'/>}
                                <ListElement 
                                    item={item.pulse} 
                                    text ='PUL' 
                                    textTooltip='Pulse'/>
                                <ListElement item={item.date}/>
                                <Icon
                                    name='close'
                                    color='#ff7f50' 
                                    onPress={() => deletePressureMeasurement(item)} />    
                            </View>}/>
                </View>
                <StatusBar style="auto" />
            </Animated.View>
        </View>
    );
}