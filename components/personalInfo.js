import React, { useState, useEffect, useRef, createRef } from 'react';
import { View, Text, Button, Animated, ScrollView, SafeAreaView } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { Icon } from 'react-native-elements';
import ActionSheet from "react-native-actions-sheet";

import { InputFields } from './inputField';
import { styles } from './personalInfoStyle';

export const PersonalInfo = props => {
    const { 
        isCreate, 
        user, 
        onCreateOrUpdate, 
        onCancel 
    } = props;
    const [firstName, setFirstName] = useState(user && user.firstName ? user.firstName : '');
    const [lastName, setLastName] = useState(user && user.lastName ? user.lastName : '');
    const [weight, setWeight] = useState(user && user.weight ? user.weight : '');
    const [age, setAge] = useState(user && user.age ? user.age : '');
    const [height, setHeight] = useState(user && user.height ? user.height : '');
    const [gender, setGender] = useState(user && user.gender ? user.gender : 'MALE');
    const fadeAnim = useRef(new Animated.Value(0)).current;
    const actionSheetRef = createRef();

    useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: 1000,
                useNativeDriver: true
            }
        ).start();
    }, [fadeAnim]);

    const openActionSheet = () => {
        actionSheetRef.current?.setModalVisible(true);
    };
    
    const setGenderAndClose = gender => {
        setGender(gender);
        actionSheetRef.current?.setModalVisible(false);
    }

    return(
        <SafeAreaView style={ styles.mainContainer }>
            <ScrollView style={ styles.mainContainer }>
                <Animated.View style={{...styles.containerMainInput, opacity: fadeAnim}}>
                    <View style={styles.firstContainer}>
                        <View style={styles.iconContainer}>
                            {gender === 'MALE' ? 
                                <Icon
                                    name='male'
                                    type='font-awesome'
                                    color='#ff5722'
                                    onPress={() => openActionSheet()} /> : 
                                <Icon
                                    name='female'
                                    type='font-awesome'
                                    color='#ff5722'
                                    onPress={() => openActionSheet()} />} 
                        </View>
                        <View style={styles.infosPeronalInfoContainer}>
                            <Text  style={styles.textInContainer}>
                                {firstName}
                            </Text>
                            <Text style={styles.textInContainer}>
                                {lastName}
                            </Text>
                        </View>
                        <View style={styles.inputsPeronalInfoContainer}>
                            <InputFields 
                                    text='First name' 
                                    textTooltip='First name'
                                    maxLen={100}
                                    itemOnchange={firstName => setFirstName(firstName)}
                                    defaultVal={firstName}/>
                        </View>
                        <View style={styles.inputsPeronalInfoContainer}>
                            <InputFields 
                                    text='Last name' 
                                    textTooltip='Last name'
                                    maxLen={100}  
                                    itemOnchange={lastName => setLastName(lastName)}
                                    defaultVal={lastName}/>
                        </View>
                        <View style={styles.inputsPeronalInfoContainer}>
                            <InputFields 
                                    text='Age' 
                                    textTooltip='Age'
                                    maxLen={3}  
                                    itemOnchange={age => setAge(age)}
                                    defaultVal={age}/>
                        </View>
                        <View style={styles.inputsPeronalInfoContainer}>
                            <InputFields 
                                    text='Weight' 
                                    textTooltip='Weight'
                                    maxLen={3}  
                                    itemOnchange={weight => setWeight(weight)}
                                    defaultVal={weight}/>
                        </View>
                        <View style={styles.inputsPeronalInfoContainer}>
                            <InputFields 
                                    text='Height' 
                                    textTooltip='Height'
                                    maxLen={3}  
                                    itemOnchange={height => setHeight(height)}
                                    defaultVal={height}/>
                        </View>
                        <View style={styles.buttonCreateUpdate}>
                            <Button
                                onPress={()=> onCreateOrUpdate({ 
                                    firstName, 
                                    lastName,
                                    age,
                                    gender,
                                    weight,
                                    height 
                                })}
                                title={isCreate ? "Create" : "Update"}
                                color="#ff5722"/> 
                        </View>
                        {!isCreate && 
                        <View style={styles.buttonBack}>
                            <Button
                                onPress={()=> onCancel()}
                                title={"Back"}
                                color="#ff5722"/> 
                        </View>}
                        <ActionSheet 
                                ref={actionSheetRef}
                                containerStyle={{ backgroundColor: '#fff' }}>
                            <View style={styles.actionSheetContainer}>
                                <View style={styles.actionSheetElementFirst}>
                                    <View style={{ paddingRight: 10 }}>
                                        <Icon
                                            name='male'
                                            type='font-awesome'
                                            color='#ff5722'
                                            onPress={() => setGenderAndClose('MALE')} />
                                    </View>
                                    <View>
                                        <Button
                                            onPress={()=> setGenderAndClose('MALE')}
                                            title={'MALE'}
                                            color="#ff5722"/>
                                    </View>
                                </View>
                                <View style={styles.actionSheetElement}>
                                    <View style={{ marginLeft: 10 }}>
                                        <Icon
                                            name='female'
                                            type='font-awesome'
                                            color='#ff5722'
                                            onPress={() => setGenderAndClose('FEMALE')} />
                                    </View>
                                    <View style={{ paddingLeft: 10 }}>
                                        <Button
                                            onPress={()=> setGenderAndClose('FEMALE')}
                                            title={'FEMALE'}
                                            color="#ff5722"/>
                                    </View>   
                                </View>
                            </View>
                        </ActionSheet>
                    </View>
                    <StatusBar style="auto" />
                </Animated.View>
            </ScrollView>
        </SafeAreaView>
    )
}