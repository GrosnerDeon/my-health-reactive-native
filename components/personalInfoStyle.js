import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        flexDirection: "column",
        backgroundColor: '#e9967a',
        marginTop: 40
    },
    containerMainInput: {
        flex: 1,
        width: '100%',
        flexDirection: "column",
        backgroundColor: '#faebd766',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: '#9e9e9e',
      },
    firstContainer: {
        flex: 1,
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
    },
    secondContainer: {
        height: 40,
        width: 250,
    },
    iconContainer: {
        marginTop: 10,
        height: 40,
        width: 250
    },
    infosPeronalInfoContainer: {
        marginTop: 10,
    },
    inputsPeronalInfoContainer: {
        height: 80,
        width: 250,
        marginTop: 10
    },
    buttonCreateUpdate: {
        height: 40,
        width: 200,
        marginTop: 40
    },
    buttonBack: {
        height: 40,
        width: 200,
    },
    textInContainer: {
        textAlign: 'center'
    },
    actionSheetContainer: {
        height: 100
    },
    actionSheetElement: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    actionSheetElementFirst: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        paddingBottom: 10,
        borderBottomWidth: 4,
        borderBottomColor: '#FFA500'
    }
});