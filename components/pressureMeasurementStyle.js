import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: "row",
        backgroundColor: '#ffe1b9',
        alignItems: 'center',
        justifyContent: 'space-around',
        padding: 4,
        borderBottomWidth: 2,
        borderBottomColor: '#e9967a'
    },
    containerMainInput: {
        width: '100%',
        height: 85,
        flexDirection: 'column'
    },
    containerMainInputSendToServer: {
        width: '100%',
        height: 80,
        flexDirection: 'column'
    },
    containerInput: {
        width: '100%',
        height: 80,
        flexDirection: "row",
        backgroundColor: '#ff7f50',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderLeftWidth: 4,
        borderRightWidth: 4,
        borderColor: "#9e9e9e",
        borderRadius: 6
    },
    containerList: {
        flex: 1,
        minWidth: '100%',
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 4,
        borderColor: '#9e9e9e',
        backgroundColor: '#faebd7',
        borderRadius: 6
    },
    mainContainer: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#e9967a',
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerPersonalInfo: {
        height: 80,
        width: '100%',
        marginTop: 40,
        flexDirection: 'row',
        backgroundColor: '#faebd766',
        alignItems: 'center',
        borderWidth: 4,
        borderColor: '#9e9e9e',
        justifyContent: 'space-around'
    },
    textContainerPersonalInfo: {
        textAlign: 'center'
    } 
});