import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    field: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        backgroundColor: '#f3f3f3',
        borderRadius: 4,
        padding: 2,
        marginRight: 10,
        marginLeft: 10
    },
    textInField: {
        paddingRight: 10
    }
  });