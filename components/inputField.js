import React from 'react';
import { View, TextInput } from 'react-native';
import { Tooltip, Text } from 'react-native-elements';

import { styles } from './inputFieldStyle';

export const InputFields = props => {
    const { 
        itemOnchange,
        text,
        textTooltip,
        defaultVal, 
        myTextInput, 
        maxLen 
    } = props;
    
    return (
        <View style={styles.field}>
            <Tooltip 
                popover={
                    <Text>
                        {textTooltip}
                    </Text>
                }>
                <Text style={styles.textInField}>
                    {text}
                </Text>
            </Tooltip>
            <TextInput
                style={{ 
                    height: 40,
                    width: '100%'
                }}
                maxLength={maxLen}
                onChangeText={itemOnchange}
                defaultValue={defaultVal}
                ref={myTextInput}
            />
        </View> 
    )
}