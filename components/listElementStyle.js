import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    field: {
        flexDirection: "row",
        alignItems: 'center',
        borderRadius: 4,
        padding: 6,
        textAlign: 'left'
    },
    textInField: {
        paddingRight: 5
    }
  });