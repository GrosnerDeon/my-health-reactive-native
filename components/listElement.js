import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { styles } from './listElementStyle';

export const ListElement = props => {
    const { 
        item,
        text, 
        textTooltip 
    } = props;

    return (
        <View style={styles.field}>
            {text && textTooltip && 
                <Text style={styles.textInField}>
                    {text}
                </Text>}
            <Text>
                {item}
            </Text>
        </View> 
    )
}