import React, { useState, useEffect } from 'react';
import { Alert, Modal, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

import { PressureMeasurement } from './pressureMeasurement';
import { PersonalInfo } from './personalInfo';
import { serviceData } from '../services/serviceData';
import { styles } from './mainScreenStyle';

export const MainScreen = () => {
    const [isLoadDonePersonalInfo, setIsLoadDonePersonalInfo] = useState(false);
    const [isPersonalAbsence, setIsPersonalAbsence] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false);
    const [user, setUser] = useState({});
    const [modalVisible, setModalVisible] = useState(false);
    const [messageToServer, setMessageToServer] = useState('');

    useEffect(() => {
        serviceData.getPersonalInfo().then(value => {
            setIsLoadDonePersonalInfo(true);

            if (value === null) {
                setIsPersonalAbsence(true);
            } else {
                setIsPersonalAbsence(false);
                setUser({ ...value, ...user })
            }
        })
    }, []);

    const conditionPersonalInfo = isLoadDonePersonalInfo && isPersonalAbsence || isUpdate;
    const conditionalPressureMeasurement = isLoadDonePersonalInfo && !isPersonalAbsence && !isUpdate;

    const onCancel = () => {
        serviceData.getPersonalInfo().then(value => {
            setIsLoadDonePersonalInfo(true);
            
            if (value === null) {
                setIsPersonalAbsence(true);
            } else {
                setIsPersonalAbsence(false);
                setUser({ ...value,...user })
                if (isUpdate) {
                    setIsUpdate(false)
                }
            }
        });
    };

    const onCreateOrUpdate = user => {
        if (user.firstName !== '' && 
            user.lastName !== '' && 
            user.age !== '' &&
            user.weight !== '' &&
            user.height !== '' &&
            !isNaN(Number(user.age)) &&
            !isNaN(Number(user.weight)) && 
            !isNaN(Number(user.height))) {
            serviceData.bloodPressureCheck(user)
            serviceData.storePersonalInfo(user).then(() => {
                serviceData.getPersonalInfo().then(value => {
                    setIsLoadDonePersonalInfo(true);
                    
                    if (value === null) {
                        setIsPersonalAbsence(true);
                    } else {
                        setIsPersonalAbsence(false);
                        setUser({ ...value,...user })
                            if (isUpdate) {
                                setIsUpdate(false)
                            }
                    }
                });
            });
        }
    };

    return(
        <View style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: '#e9967a'}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>
                            {messageToServer}
                        </Text>
                        <Icon
                            name='close'
                            color='#ff7f50' 
                            onPress={() => setModalVisible(!modalVisible)} /> 
                    </View>
                </View>
            </Modal>
            {conditionPersonalInfo && 
                <PersonalInfo 
                        isCreate={isPersonalAbsence} 
                        user={user}
                        onCancel={() => onCancel()}
                        onCreateOrUpdate={(user) => onCreateOrUpdate(user)}/>}
            {conditionalPressureMeasurement && 
                <PressureMeasurement 
                                user={user}
                                onModalViewOpen={message => {
                                    setModalVisible(true)
                                    setMessageToServer(message)
                                }} 
                                onUpdateUser={() => {
                                    setIsUpdate(true);
                                }} />}
        </View>
  )
}