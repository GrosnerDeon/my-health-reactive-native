import AppLoading from 'expo-app-loading';
import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';
import { Animated, StyleSheet, View } from 'react-native';
import React from 'react';

const AnimatedSplashScreen = ({ children, image }) => {
    const animation = React.useMemo(() => new Animated.Value(1), []);
    const [isAppReady, setAppReady] = React.useState(false);
    const [isSplashAnimationComplete, setAnimationComplete] = React.useState(
      false
    );
  
    React.useEffect(() => {
      if (isAppReady) {
        Animated.timing(animation, {
          toValue: 0,
          duration: 200,
          useNativeDriver: true,
        }).start(() => setAnimationComplete(true));
      }
    }, [isAppReady]);
  
    const onImageLoaded = React.useMemo(() => async () => {
      try {
        await SplashScreen.hideAsync();
        // Load stuff
        await Promise.all([]);
      } catch (e) {
        // handle errors
      } finally {
        await new Promise(resolve => setTimeout(resolve, 2500))
  
        setAppReady(true);
      }
    });
  
    return (
      <View style={{ flex: 1 }}>
        {isAppReady && children}
        {!isSplashAnimationComplete && (
          <Animated.View
            pointerEvents="none"
            style={[
              StyleSheet.absoluteFill,
              {
                backgroundColor: "#e9967a",
                opacity: animation,
              },
            ]}
          >
            <Animated.Image
              style={{
                width: "100%",
                height: "100%",
                resizeMode: "contain",
                transform: [
                  {
                    scale: animation,
                  },
                ],
              }}
              source={image}
              onLoadEnd={onImageLoaded}
              fadeDuration={0}
            />
          </Animated.View>
        )}
      </View>
    );
  }

export const AnimatedAppLoader = ({ children, image }) => {
    const [isSplashReady, setSplashReady] = React.useState(false);

    const startAsync = React.useMemo(
      // If you use a local image with require(...), use `Asset.fromModule`
      () => () => Asset.fromModule(image).downloadAsync(),
      [image]
    );
  
    const onFinish = React.useMemo(() => setSplashReady(true), []);
  
    if (!isSplashReady) {
      return (
        <AppLoading
          // Instruct SplashScreen not to hide yet, we want to do this manually
          autoHideSplash={false}
          startAsync={startAsync}
          onError={console.error}
          onFinish={onFinish}
        />
      );
    }
  
    return <AnimatedSplashScreen image={image}>{children}</AnimatedSplashScreen>;
}